const _ = require('lodash');
const snmp = require('net-snmp');
const async = require('async');
const net = require('net');
const path = require('path')
const moment = require('moment');
require('dotenv').config({ path: path.resolve(process.cwd(), 'env') });

const now = () => moment().format('Y-MM-DD HH:mm:ss');

const logError = (...args) => console.error(now(), ...args);
const log = (...args) => console.log(now(), ...args);

const toAuthProtocol = (authType) => {
    switch (authType) {
        case 'SHA': return snmp.AuthProtocols.sha;
        case 'MD5': return snmp.AuthProtocols.md5;
        default: return null;
    }
};

const toPrivProtocol = (privType) => {
    switch (privType) {
        case 'AES': return snmp.PrivProtocols.aes;
        case 'DES': return snmp.PrivProtocols.des;
        default: return null;
    }
};

const toSecurityLevel = (authProtocol, privProtocol) => {
    if (authProtocol && privProtocol) {
        return snmp.SecurityLevel.authPriv;
    } else if (authProtocol && !privProtocol) {
        return snmp.SecurityLevel.authNoPriv;
    } else if (!authProtocol && !privProtocol) {
        return snmp.SecurityLevel.noAuthNoPriv;
    } else {
        return null;
    }
}

const toUser = () => {
    const authProtocol = toAuthProtocol(process.env.AUTH_TYPE);
    const privProtocol = toPrivProtocol(process.env.PRIV_TYPE);
    return {
        name: process.env.USER_NAME,
        level: toSecurityLevel(authProtocol, privProtocol),
        authProtocol,
        authKey: process.env.AUTH_KEY,
        privProtocol,
        privKey: process.env.PRIV_KEY,
    }
}

const snmpget = async (session, oid) => {
    return new Promise((resolve, reject) => {
        session.get([oid], (error, varbinds) => {
            if (error) reject(error);
            resolve(varbinds);
        })
    }).catch(e => {
        logError(`Req Error: ${session.target} Get ${oid} => ${e.message}`);
    });
}

const printUsage = (scriptName) => {
    console.log(`USAGE: node ${path.basename(scriptName)} <IP> <interval: 단위 ms> <quantity>`)
}

const sessionErrorHandler = function (error) {
    logError(`Session Error ${this.target}  => ${error.message}`);
    this.close();
}

const printVarbinds = (varbinds) => _.isArray(varbinds) && varbinds.forEach(varbind => log(`${varbind.oid} => ${varbind.value}`));

const createTask = (session, oid) => async () => snmpget(session, oid).then(varbinds => printVarbinds(varbinds));

const _request = (interval, concurrency, sessionArgs) => {
    console.log(sessionArgs);
    console.log(`Target: ${sessionArgs[0]}, Interval: ${interval} ms, Quantity: ${concurrency}`);
    let attempts = 0;
    const _inner = async () => {
        const session = snmp.createV3Session(...sessionArgs);
        session.on('error', sessionErrorHandler.bind(session));
        const tasks = Array(+concurrency).fill(createTask(session, process.env.OID));
        await async.parallelLimit(tasks, concurrency);
        log('<----------------------------------------------------------', ++attempts);
        setTimeout(_inner, interval);
    }

    _inner();
}

(async (argv) => {
    const [, scriptName, ipAddr, interval, concurrency] = argv;

    if (argv.length !== 5) {
        printUsage(scriptName);
        process.exit();
    }

    if (!net.isIPv4(ipAddr)) {
        console.error(`${ipAddr} 은 유효한 IP 주소가 아닙니다.`);
        printUsage(scriptName);
        process.exit();
    }

    const options = { version: snmp.Version3 };
    _request(interval, concurrency, [ipAddr, toUser(), options]);
})(process.argv);